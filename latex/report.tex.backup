\documentclass[a4paper,12pt]{article}
\usepackage{graphicx}
\usepackage{amsmath, amsfonts}
\usepackage{amssymb}

\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{lmodern}
\usepackage{textcomp}

\usepackage{natbib}

% \usepackage{babel}

% \usepackage{titlesec}

\usepackage[top=1.2in,bottom=1.2in,left=3.cm,right=3.cm,a4paper]{geometry}

\usepackage[font=footnotesize]{caption}

\usepackage{xcolor}

\usepackage{hyperref}

\hypersetup{
    colorlinks,
    linkcolor={red!50!black},
    citecolor={blue!50!black},
    urlcolor={blue!99!black}
}

\usepackage{titling}

\pretitle{%
\includegraphics[width=0.3\linewidth]{../fig/logo_UGA}
\vspace{2cm}
\begin{center}
\LARGE
}
\posttitle{\end{center}}
\postdate{\par\end{center}\vspace{12cm}~}

\usepackage[nottoc,numbib]{tocbibind}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{INSTABILITIES AND TURBULENCE}
\author{Muhammad Mustapha MUSA}

% to define new commands
\newcommand{\R}{\mathcal{R}}
\newcommand{\mean}[1]{\langle #1 \rangle}

% to show the pieces of advice
\newcommand{\advice}[1]{{\it #1}}
% to hide the pieces of advice
% \newcommand{\advice}[1]{}

\begin{document}

\renewcommand{\labelitemi}{$\bullet$}

\maketitle

\tableofcontents

\newpage

\section{PREY-PREDATOR MODEL}

Prey-Predator Model: The prey-predator model is a method of studying a system with two degrees of freedom where the variables are X(t) and Y(t). 
The model has stationary points when dx/dt=0 and dy/dt=0 
The purpose of the report is to discuss the effect of time stepping on the model and the stability of the stationary points. the equations cqn be written as 

\begin{align*}
\dot X &= A X - B XY, \\
\dot Y &= -C Y + D XY,
\end{align*}
where $X(t)$ and $Y(t)$ are two functions of time representing species of preys
and predators, respectively. The parameters $A$, $B$, $C$ and $D$ are real and
positive.


\subsection{Effect of Time Stepping on the Prey-Predator Model}

Analysis of the effect of time stepping on the model shows that as the time steping is decreased, 
the eliptic shape of the graph becomes rougher.Also, the period of oscillation about the fixed point reduced. the the time setpping was increased to 40, 
the period of oscillation increased. The oscillation of both prey and predator about their respective fixed points is a little bit out of phase this is because the predator's 
population depends on the preys' population. When the Prey increases, the predators also increases and hence the population of the prey is consumed by predators.
This reduces the amount of preys. When the populatons of the preys reduces, it also affects the population of the predators as well.
The effect of time stepping has effect on the period of the oscllation. as the time steping is icreased, the oscillation of the functions increased, so does the amplitude. When the 
time stepping is reduced, the period also reduces.
This is shown in the figures below.


\begin{figure}[!ht]
\centering
\includegraphics[width=0.7\linewidth]{../STPT17.png}
\caption{Increased Time Stepping 1}
%in the figures. It has to be approximately the same than for the other texts in
%the report. }
%\label{fig_simple}
\end{figure}

\begin{figure}[!ht]
\centering
\includegraphics[width=0.7\linewidth]{../STPT18.png}
\caption{Increased Time step t=1}
%in the figures. It has to be approximately the same than for the other texts in
%the report. }
%\label{fig_simple}
\end{figure}

\begin{figure}[!ht]
\centering
\includegraphics[width=0.7\linewidth]{../STPT19.png}
\caption{Reduced Time Step t=0.01}
%in the figures. It has to be approximately the same than for the other texts in
%the report. }
%\label{fig_simple}
\end{figure}

\begin{figure}[!ht]
\centering
\includegraphics[width=0.7\linewidth]{../STPT20.png}
\caption{Reduced Time Step t=0.01}
%in the figures. It has to be approximately the same than for the other texts in
%the report. }
%\label{fig_simple}
\end{figure}

\subsection{Stability of the Fixed Points}
Now, we analyse the stability of the fixed point by linearising the equations using partial derivatives. The produces a jacobian matrix of the model.

The first fixed point is at X,Y = (0,0) when there is extinction of both populations. This fixed is unstable becquse it leads to extinction of both species.

\break
Analysing the second fixed point. 
\begin{align*}
\dot x &= Ax, \\
\dot y &= -Cy,
\end{align*}
and can be written as $\dot V = J V$, with
$$
V =
  \begin{pmatrix}
    x \\
    y
  \end{pmatrix}
\quad\text{and}\quad
J =
  \begin{pmatrix}
    A & 0 \\
    0 & -C
  \end{pmatrix}.
$$
$J$ is the Jacobian matrix of the system. We need to find eigenvalues and
eigenmodes of this matrix, i.e. vector $V_i$ such that $\sigma_i V_i = J
V_i$. Since, $J$ is in this case already diagonal, it is trivial. The
eigenvalues are $\sigma_1 = A$ and $\sigma_2 = -C$ and the eigenvectors are
$$
V_1 =
  \begin{pmatrix}
    1 \\
    0
  \end{pmatrix}
\quad\text{and}\quad
V_2 =
  \begin{pmatrix}
    0 \\
    1
  \end{pmatrix}.
$$
Since $A$ and $C$ are both positive, this fixed point is unstable and it is a
saddle point ($\sigma_1 > 0$ and $\sigma_2 < 0$).

{\bf We now consider an infinitesimal perturbation around the second fixed
point $(X,\ Y) = (C/D,\ A/B)$}.  The linearized equations can be written as
$\dot V = J V$ with the Jacobian matrix
$$
 J =
  \begin{pmatrix}
    0 & -\frac{CB}{D} \\
    \frac{AD}{B} & 0
  \end{pmatrix}.
$$
This time, it is not diagonal so we have to work a little bit more. The
solvability condition is
$$ \det (J - \sigma_i \Id) = 0,
$$
with $\Id$ the identity matrix, which gives us $\sigma_\pm = \pm i \sqrt{AC}$,
which means that the fixed point is elliptic (characterizes the shape of
the orbits close to the fixed point). By injecting the value of the eigenvalue
in the linearized equations, we find the eigenvectors:


\begin{figure}[!ht]
\centering
\includegraphics[width=0.7\linewidth]{../STPT17.png}
\caption{Increased Time Stepping 1}
%in the figures. It has to be approximately the same than for the other texts in
%the report. }
%\label{fig_simple}
\end{figure}

\begin{figure}[!ht]
\centering
\includegraphics[width=0.7\linewidth]{../STPT18.png}
\caption{Increased Time step t=1}
%in the figures. It has to be approximately the same than for the other texts in
%the report. }
%\label{fig_simple}
\end{figure}

\begin{figure}[!ht]
\centering
\includegraphics[width=0.7\linewidth]{../STPT19.png}
\caption{Reduced Time Step t=0.01}
%in the figures. It has to be approximately the same than for the other texts in
%the report. }
%\label{fig_simple}
\end{figure}

\begin{figure}[!ht]
\centering
\includegraphics[width=0.7\linewidth]{../STPT20.png}
\caption{Reduced Time Step t=0.01}
%in the figures. It has to be approximately the same than for the other texts in
%the report. }
%\label{fig_simple}
\end{figure}

\section{ISOCONTOURS}
The isocolines indicates the behavior of the model with different period. It shows that the prey-predator model behaves the same regardless of the population of the species.

\begin{figure}[!ht]
\centering
\includegraphics[width=0.7\linewidth]{../isobars.png}
\caption{Isocontours}
%in the figures. It has to be approximately the same than for the other texts in
%the report. }
%\label{fig_simple}
\end{figure}


\section{SYSTEM WITH THREE DEGREES OF FREEDOM: LORENTZ MODEL}

In this model, the system has three degrees of freedom. At any particular point, there is no predictability of what path the system will follow when parturbed.
This is called the butterfly effect and the system is highly sensitive to initial conditions. Hence, we can not guess the path of the system wth certainty, and the system is not having periodic orbit.
This is an example of a chaotic system.

\begin{figure}[!ht]
\centering
\includegraphics[width=0.7\linewidth]{../CHAOS.png}
\caption{The thoery of Chaos}
%in the figures. It has to be approximately the same than for the other texts in
%the report. }
%\label{fig_simple}
\end{figure}

\begin{figure}[!ht]
\centering
\includegraphics[width=0.7\linewidth]{../CHAOS vsTIME.png}
\caption{Space Versus Time}
%in the figures. It has to be approximately the same than for the other texts in
%the report. }
%\label{fig_simple}
\end{figure}


\bibliographystyle{jfm}
\bibliography{./biblio}

\end{document}
