"""
To be used with ipython --matplotlib

run job_sim_predaprey1.py

"""
import matplotlib.pyplot as plt

from fluidsim.solvers.models0d.predaprey.solver import Simul

params = Simul.create_default_params()

params.time_stepping.deltat0 = 0.01
params.time_stepping.t_end = 20

params.output.periods_print.print_stdout = 0.01

sim = Simul(params)

sim.state.state_phys.set_var('X', sim.Xs + 2.)
sim.state.state_phys.set_var('Y', sim.Ys + 1.)

# sim.output.phys_fields.plot()
sim.time_stepping.start()


sim.output.print_stdout.plot_XY()

fig = plt.gcf()
fig.savefig("STPT19.png")
sim.output.print_stdout.plot_XY_vs_time()
fig = plt.gcf()
fig.savefig("STPT20.png")
