
import numpy as np
import matplotlib.pyplot as plt

from fluidsim.solvers.models0d.predaprey.solver import Simul
params = Simul.create_default_params()
sim=Simul(params)

xs = np.linspace(0.01, 5, 50)
ys = np.linspace(0.01, 5, 50)

Xs, Ys = np.meshgrid(xs, ys)

potential = (params.C * np.log(Xs) -
             params.D * Xs +
             params.A * np.log(Ys) -
             params.B * Ys)

plt.figure()
ax = plt.gca()

ax.contour(Xs, Ys, potential, 50)
ax.plot(sim.Xs, simYs, "k+")

ax.set_xlabel('$X$')
ax.set_ylabel('$y$')

ax.set_title('Isolines of potential for the prey-predator model')

plt.show()


sim.output.print_stdout.plot_XYZ()
fig = plt.gcf()
fig.savefig("ISOBARS.png")
